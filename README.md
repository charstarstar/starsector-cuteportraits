Note:
I won't be maintaining this.  Feel free to copy/modify/update for later
versions as needed.

Cute portraits for Starsector players and pirates.

Because getting jumped by a pirate armada isn't upsetting enough already.

Installation:

1. Unzip to <starsector-program-dir>\mods\.

2. This should create a mod directory named "CutePortraits".

By default, these portraits are added only to the core factions.

To add the portraits to other factions:

1. In the CutePortraits mod directory, go to the "CutePortraits\data\world\" directory.

2. Move the appropriate faction files from the "factions.UNUSED\" directory to the "factions\" directory.


Disclaimer :
All images are copyright to their respective owners.

